<?php
// Routes

$app->get('/',function( $request, $response){
    // $this->session->destroy();
    $this->session->set('last_url', '/');
    $categorys_mapper = new CategoryMapper($this->db);
    $categorys = $categorys_mapper->getCategory();
    $params = $request->getQueryParams(); 
    
    $param_keyword = isset($params['keyword'])? $params['keyword']:'';
    $param_category = isset($params['category'])? $params['category']:'';

    $job_query = "SELECT j.id, j.job_title, j.company, j.place, t.job_type, c.category, j.salary_mon_min, j.salary_mon_max, e.experience
                  from jobs j
                  join job_type t on (t.id = j.job_type_id) 
                  join category c on (c.id = j.category_id)
                  join experience e on (e.id = j.experience_id)
                  join users u on (u.id = j.owener_id)
                  where j.visible=1 and j.verify=1 and j.date_expired > ".date("Y-m-d");
    if( $param_keyword != '' || $param_category != ''){
        if( $param_keyword != ''){
            $job_query .= " and (j.job_title LIKE '%$param_keyword%'
                            OR j.company LIKE '%$param_keyword%'
                            OR j.place LIKE '%$param_keyword%'
                            OR t.job_type LIKE '%$param_keyword%'
                            OR e.experience LIKE '%$param_keyword%'
                            OR c.category LIKE '%$param_keyword%'
                            OR j.salary_mon_min LIKE '%$param_keyword%'
                            OR j.salary_mon_max LIKE '%$param_keyword%'
                            )"; 
        }
        if( $param_category != ''){ $job_query .= " and c.id = $param_category" ;}     
    } 
    $job_query .= ' ORDER BY j.date_mod DESC';             
    // print($job_query);
    $Paginator  = new Paginator($this->db, $job_query);
    
    $limit = ( isset( $params['limit'] ) ) ? $params['limit'] : 5;
    $page = ( isset( $params['page'] ) ) ? $params['page'] : 1;
    $links = ( isset( $params['links'] ) ) ? $params['links'] : 7;
    $results    = $Paginator->getData( $limit, $page, $links, "JobsEntity" );
    $jobs = $results->data;

    $response = $this->view->render($response, "index.phtml", ["categorys" => $categorys, "result"=>$results, "jobs"=>$jobs, "Paginator"=> $Paginator, "hidden_category" => $param_category, "url"=>$request->getUri()->getQuery(),"base_url"=>$request->getUri()->getBaseUrl(),"path_url"=>$request->getUri() ]);
    return $response;
});
$app->get('/jobs/new', function ( $request,  $response) {
    if( !$login = $this->session->get('login') ){
        $response = $response->withRedirect("/login");
        return $response;
    }
    $categorys_mapper = new CategoryMapper($this->db);
    $categorys = $categorys_mapper->getCategory();

    $job_type_mapper = new JobTypeMapper($this->db);
    $job_types = $job_type_mapper->getJobType();

    $experience_mapper = new ExperienceMapper($this->db);
    $experiences = $experience_mapper->getExperience();

    $this->session->set('last_url', '/jobs/new');
    $response = $this->view->render($response, "jobsnew.phtml", ["categorys" => $categorys,"base_url"=>$request->getUri()->getBaseUrl(), "job_types"=>$job_types, "experiences"=>$experiences ,"path_url"=>$request->getUri()]);
    return $response;
});
$app->post('/jobs/new', function ( $request,  $response) {
    $data = $request->getParsedBody();
    $this->session->set('last_url', '/jobs/new');

    $jobs_data = [];
    $jobs_data['job_title'] = $data['job_title'];
    $jobs_data['job_description'] = $data['job_description'];
    $jobs_data['place'] = $data['place'];
    $jobs_data['company'] = $data['company'];
    $jobs_data['website'] = $data['website'];
    $jobs_data['salary_mon_min'] = (int)$data['salary_mon_min'];
    $jobs_data['salary_mon_max'] = (int)$data['salary_mon_max'];
    // $jobs_data['salary_year_max'] = (int)$data['salary_year_max'];
    $jobs_data['job_type'] = (int)$data['job_type_id'];
    $jobs_data['experience'] = (int)$data['experience_id'];
    $jobs_data['category'] = (int)$data['category_id'];
    $jobs_data['owener_id'] = (int)$_SESSION['user_id'];
    $jobs_data['date_expired'] = $data['date_expired'];
    $jobs_data['date_post'] = date('Y-m-d');
    // print_r($jobs_data);
    $jobs_data['logo_path'] = '';
    $jobs_data['company_depart'] = $data['company_depart'];
    // TODO check file size 
    $files = $request->getUploadedFiles();
    // print_r($files);
    if (!empty($files['logo_path'])) {
        $newfile = $files['logo_path'];
        if ( $newfile->getError() === UPLOAD_ERR_OK) {
            // $uploadFileType = $newfile->getClientMediaType();
            // // print($uploadFileType);
            // switch ($uploadFileType) {
            //     case 'image/png':
            //         break;
            //     case 'image/jpeg':
            //         break;
            //     case 'image/gif':
            //         break;
            //     default:
            //         return false;
            // }

            $uploadFileName = $newfile->getClientFilename();
            $target_path = generateRandomString().'_'.$uploadFileName;
            $uploaded_image_path = "./job_logo/". $target_path;
            $newfile->moveTo($uploaded_image_path);

            $thumbnail_image_path = preg_replace('{\\.[^\\.]+$}', '.jpg', $target_path);
            $result = square_thumbnail_with_proportion($uploaded_image_path, "./job_logo_crop/". $thumbnail_image_path,600);
            $jobs_data['logo_path'] = $thumbnail_image_path;
        }
    }
    print_r($jobs_data);
    $jobs = new JobsEntity($jobs_data);
    $jobs_mapper = new JobsMapper($this->db);
    $jobs_mapper->save($jobs);
    // $response = $response->withRedirect("/");
    $response = $response->withRedirect("/jobs/dashboard");
    return $response;
});
$app->post('/jobs/apply', function ( $request,  $response) {
    $data = $request->getParsedBody();    
    $this->session->set('last_url', '/jobs/apply');

    // print_r($data);

    if( (isset($data['resume_delete']) && ($data['resume_delete']=='on')) && $data['cv_path'] ){
        $data['cv_path'] = '';
    }
    // TODO check file size 
    $files = $request->getUploadedFiles();
    if (!empty($files['newfile'])) {
        $newfile = $files['newfile'];
        if ($newfile->getError() === UPLOAD_ERR_OK) {
            $uploadFileName = $newfile->getClientFilename();
            $target_path = generateRandomString().'_'.$uploadFileName;
            $newfile->moveTo("./application/". $target_path );
            $data['cv_path'] = $target_path;
        }
    }
    $data['send_time'] = date("Y-m-d H:i:s");
    $applications = new ApplicationEntity($data);
    $application_mapper = new ApplicationMapper($this->db);
    $application_mapper->save($applications);

    $job_id = $data['job_id'];
    $mapper = new JobsMapper($this->db);
    $owner = $mapper->getOwnerbyJobId($job_id);
    // print_r($owner);
    // 
    $english_mapper = new EnglishMapper($this->db);
    $english = $english_mapper->getEnglishById($data['english_ability_id']);
    $data['english_ability_id'] = $english['english'];
    $education_mapper = new EducationMapper($this->db);
    $education = $education_mapper->getEducationById($data['highest_education_id']);
    $data['highest_education_id'] = $education['highest_education'];
    
    print_r($data);
    $englishs = $english_mapper->getEnglish();
    $english_opt = '（';
    foreach($englishs as $english): $english_opt .= $english->getEnglish().'/'; endforeach;
    $english_opt = substr($english_opt,0,-1) . '）';

    $educations = $education_mapper->getEducation();
    $educations_opt = '（';
    foreach($educations as $education): $educations_opt .= $education->getEducation().'/'; endforeach;
    $educations_opt = substr($educations_opt,0,-1) . '）';

    send_apply_to_job_owner($data,$owner,$english_opt,$educations_opt);

    $response = $response->withRedirect("/");
    // $response = $response->withRedirect("/jobs/dashboard");
    // return $response;
});
$app->get('/jobs/dashboard',function( $request, $response){
    // TODO check admin
    if( !$login = $this->session->get('login') ){
        $response = $response->withRedirect("/login");
        return $response;
    }
    $categorys_mapper = new CategoryMapper($this->db);
    $categorys = $categorys_mapper->getCategory();

    if( $user_id = $this->session->get('user_id') ){
        $jobs_mapper = new JobsMapper($this->db);
        $jobs = $jobs_mapper->getJobsbyOwner($user_id);   

        $response = $this->view->render($response, "dashboard.phtml", ["jobs_mapper"=>$jobs_mapper, "categorys" => $categorys, "result"=>count($jobs),"jobs"=>$jobs, "url"=>$request->getUri()->getQuery(),"base_url"=>$request->getUri()->getBaseUrl(),"path_url"=>$request->getUri() ]);
    }else{
        $response = $response->withRedirect("/login");
        return $response;
    }
    
    return $response;
});
$app->get('/jobs/admin/edit/{id}', function ($request, $response, $args) {
    // TODO check admin
    if( !$login = $this->session->get('login') ){
        $response = $response->withRedirect("/login");
        return $response;
    }    
    $categorys_mapper = new CategoryMapper($this->db);
    $categorys = $categorys_mapper->getCategory();

    $job_id = (int)$args['id'];
    $mapper = new JobsMapper($this->db);
    $job = $mapper->getJobById($job_id);

    $job_type_mapper = new JobTypeMapper($this->db);
    $job_types = $job_type_mapper->getJobType();

    $experience_mapper = new ExperienceMapper($this->db);
    $experiences = $experience_mapper->getExperience();
    $this->session->set('last_url', '/jobs/admin/edit');
    $response = $this->view->render($response, "jobedit_admin.phtml", ["categorys" => $categorys, "job_types"=>$job_types, "experiences"=>$experiences, "base_url"=>$request->getUri()->getBaseUrl(),"job" => $job[0],"path_url"=>$request->getUri()]);
    return $response;
});
$app->get('/jobs/edit/{id}', function ($request, $response, $args) {
    
    // TODO 
    // check {id} belong to this user 
    if( !$login = $this->session->get('login') ){
        $response = $response->withRedirect("/login");
        return $response;
    }
    $categorys_mapper = new CategoryMapper($this->db);
    $categorys = $categorys_mapper->getCategory();

    $job_id = (int)$args['id'];
    $mapper = new JobsMapper($this->db);
    $job = $mapper->getJobById($job_id);

    $job_type_mapper = new JobTypeMapper($this->db);
    $job_types = $job_type_mapper->getJobType();

    $experience_mapper = new ExperienceMapper($this->db);
    $experiences = $experience_mapper->getExperience();
    $this->session->set('last_url', '/jobs/edit');
    $response = $this->view->render($response, "jobedit.phtml", ["categorys" => $categorys, "job_types"=>$job_types, "experiences"=>$experiences, "base_url"=>$request->getUri()->getBaseUrl(),"job" => $job[0],"path_url"=>$request->getUri()]);
    return $response;
});

$app->post('/jobs/status/edit', function ($request, $response, $args) {
    $data = $request->getParsedBody();
    $this->session->set('last_url', '/jobs/admin/edit');
    $jobs_data = [];
    $jobs_data['id'] = $data['id'];
    $jobs_data['visible'] = $data['visible'];

    $jobs = new JobsEntity($jobs_data);
    $jobs_mapper = new JobsMapper($this->db);
    $re = $jobs_mapper->update_status($jobs);

    // $response = $response->withRedirect("/jobs/admin/edit/".$jobs_data['id']);
    // $response = $response->withRedirect("/jobs/dashboard");
    return $response->withJson($re);
});
$app->post('/jobs/admin/edit', function ($request, $response, $args) {

    $data = $request->getParsedBody();
    $this->session->set('last_url', '/jobs/admin/edit');

    $jobs_data = [];
    $jobs_data['id'] = $data['id'];
    $jobs_data['job_title'] = $data['job_title'];
    $jobs_data['job_description'] = $data['job_description'];
    $jobs_data['place'] = $data['place'];
    $jobs_data['company'] = $data['company'];
    $jobs_data['website'] = $data['website'];
    $jobs_data['salary_mon_min'] = (int)$data['salary_mon_min'];
    $jobs_data['salary_mon_max'] = (int)$data['salary_mon_max'];
    // $jobs_data['salary_year_max'] = (int)$data['salary_year_max'];
    $jobs_data['job_type'] = (int)$data['job_type_id'];
    $jobs_data['experience'] = (int)$data['experience_id'];
    $jobs_data['category'] = (int)$data['category_id'];
    $jobs_data['verify'] = (int)$data['verify_id'];
    $jobs_data['visible'] = (int)$data['visible_id'];
    $jobs_data['date_expired'] = $data['date_expired'];
    $jobs_data['company_depart'] = $data['company_depart'];
    $jobs_data['logo_path_tmp'] = $data['logo_path'];
    $jobs_data['logo_path'] = '';

    // TODO check file size 
    $files = $request->getUploadedFiles();
    // print_r($files);/
    if (!empty($files['newfile'])) {
        $newfile = $files['newfile'];
        // print_r($newfile);
        if ($newfile->getError() === UPLOAD_ERR_OK) {
            // $uploadFileName = $newfile->getClientFilename();
            // $target_path = generateRandomString().'_'.$uploadFileName;
            // print("./job_logo/". $target_path);
            // $newfile->moveTo("./job_logo/". $target_path );
            // $jobs_data['logo_path'] = $target_path;

            $uploadFileName = $newfile->getClientFilename();
            $target_path = generateRandomString().'_'.$uploadFileName;
            $uploaded_image_path = "./job_logo/". $target_path;
            $newfile->moveTo($uploaded_image_path);

            $thumbnail_image_path = preg_replace('{\\.[^\\.]+$}', '.jpg', $target_path);
            $result = square_thumbnail_with_proportion($uploaded_image_path, "./job_logo_crop/". $thumbnail_image_path,200);
            $jobs_data['logo_path'] = $thumbnail_image_path;
        }
    }
    if( (isset($data['logo_delete']) && ($data['logo_delete']=='on')) || $jobs_data['logo_path'] ){
        $file_path = $_SERVER['DOCUMENT_ROOT'] .'/job_logo_crop/'.$jobs_data['logo_path_tmp'];
        // print($file_path);
        if(is_file($file_path)){
            unlink($file_path);
        }
    }else{
        $jobs_data['logo_path'] = $jobs_data['logo_path_tmp'];
    }

    $jobs = new JobsEntity($jobs_data);
    $jobs_mapper = new JobsMapper($this->db);
    $jobs_mapper->update($jobs);

    $response = $response->withRedirect("/jobs/admin/edit/".$jobs_data['id']);
    // $response = $response->withRedirect("/jobs/dashboard");
    return $response;
});
$app->post('/jobs/edit', function ($request, $response, $args) {
    $data = $request->getParsedBody();
    $this->session->set('last_url', '/jobs/edit');

    $jobs_data = [];
    $jobs_data['id'] = $data['id'];
    $jobs_data['job_title'] = $data['job_title'];
    $jobs_data['job_description'] = $data['job_description'];
    $jobs_data['place'] = $data['place'];
    $jobs_data['company'] = $data['company'];
    $jobs_data['website'] = $data['website'];
    $jobs_data['salary_mon_min'] = (int)$data['salary_mon_min'];
    $jobs_data['salary_mon_max'] = (int)$data['salary_mon_max'];
    $jobs_data['job_type'] = (int)$data['job_type_id'];
    $jobs_data['experience'] = (int)$data['experience_id'];
    $jobs_data['category'] = (int)$data['category_id'];
    $jobs_data['visible'] = ($data['visible']=='on')?1:0;
    $jobs_data['verify'] = 0;
    $jobs_data['date_expired'] = $data['date_expired'];
    $jobs_data['company_depart'] = $data['company_depart'];

    $jobs_data['logo_path_tmp'] = $data['logo_path'];
    $jobs_data['logo_path'] = '';

    $files = $request->getUploadedFiles();
    // print_r($files);/
    // TODO check file size 
    if (!empty($files['newfile'])) {
        $newfile = $files['newfile'];
        // print_r($newfile);
        if ($newfile->getError() === UPLOAD_ERR_OK) {
            $uploadFileName = $newfile->getClientFilename();
            $target_path = generateRandomString().'_'.$uploadFileName;
            $uploaded_image_path = "./job_logo/". $target_path;
            $newfile->moveTo($uploaded_image_path);

            $thumbnail_image_path = preg_replace('{\\.[^\\.]+$}', '.jpg', $target_path);
            $result = square_thumbnail_with_proportion($uploaded_image_path, "./job_logo_crop/". $thumbnail_image_path,600);
            $jobs_data['logo_path'] = $thumbnail_image_path;
        }
    }
    if( (isset($data['logo_delete']) && ($data['logo_delete']=='on')) || $jobs_data['logo_path'] ){
        $file_path = $_SERVER['DOCUMENT_ROOT'] .'/job_logo_crop/'.$jobs_data['logo_path_tmp'];
        // print($file_path);
        if(is_file($file_path)){
            unlink($file_path);
        }
    }else{
        $jobs_data['logo_path'] = $jobs_data['logo_path_tmp'];
    }
    // print_r($jobs_data);
    $jobs = new JobsEntity($jobs_data);
    $jobs_mapper = new JobsMapper($this->db);
    $jobs_mapper->update($jobs);

    $response = $response->withRedirect("/jobs/edit/".$jobs_data['id']);
    return $response;
});

$app->get('/jobs/dashboard/admin',function( $request, $response){
    // TODO check admin
    if( !$login = $this->session->get('login') ){
        $response = $response->withRedirect("/login");
        return $response;
    }
    $categorys_mapper = new CategoryMapper($this->db);
    $categorys = $categorys_mapper->getCategory();
    $params = $request->getQueryParams(); 
    $param_keyword = isset($params['keyword'])? $params['keyword']:'';
    
    $job_query = "SELECT j.id, j.job_title, j.company, j.place, t.job_type, j.visible, j.verify,j.date_mod,j.date_post,j.date_expired, u.name, u.email
                  from jobs j
                  join job_type t on (t.id = j.job_type_id)
                  join users u on (u.id = j.owener_id)";
    if( $param_keyword != ''){
        $job_query .= " and (j.job_title LIKE '%$param_keyword%'
                        OR j.company LIKE '%$param_keyword%'
                        OR j.place LIKE '%$param_keyword%'
                        OR t.job_type LIKE '%$param_keyword%'
                        OR u.name LIKE '%$param_keyword%'
                        OR u.email LIKE '%$param_keyword%')"; 
    }
    $job_query .= ' ORDER BY j.date_mod DESC';              
    // print($job_query);
    $Paginator  = new Paginator($this->db, $job_query);
    
    $limit = ( isset( $params['limit'] ) ) ? $params['limit'] : 10;
    $page = ( isset( $params['page'] ) ) ? $params['page'] : 1;
    $links = ( isset( $params['links'] ) ) ? $params['links'] : 7;
    $results  = $Paginator->getData( $limit, $page, $links, "JobsEntity" );
    $jobs = $results->data;

    $job_mapper = new JobsMapper($this->db);
    // $job = $mapper->getJobById($job_id);

    $response = $this->view->render($response, "dashboard_admin.phtml", ["categorys" => $categorys, "result"=>$results,"job_mapper"=>$job_mapper,"jobs"=>$jobs, "Paginator"=> $Paginator, "path"=>$request->getUri()->getPath(),"url"=>$request->getUri()->getQuery(),"base_url"=>$request->getUri()->getBaseUrl(),"path_url"=>$request->getUri() ]);

    return $response;
});


$app->get('/jobs/{id}', function ($request, $response, $args) {
    // TODO check if !exist => not found 
    $categorys_mapper = new CategoryMapper($this->db);
    $categorys = $categorys_mapper->getCategory();

    $job_id = (int)$args['id'];
    $mapper = new JobsMapper($this->db);
    $job = $mapper->getJobByIdLimitVisible($job_id);
    
    // print_r($job);
    if( isset($job['error'])){
        // print($job['error']);     
        return $response = $this->view->render($response, "404.phtml", ["categorys" => $categorys,"path"=>$request->getUri()->getPath(),"url"=>$request->getUri()->getQuery(),"base_url"=>$request->getUri()->getBaseUrl() ,"error_msg"=>$job['error'],"path_url"=>$request->getUri()]);
    }

    $english_mapper = new EnglishMapper($this->db);
    $englishs = $english_mapper->getEnglish();
    $education_mapper = new EducationMapper($this->db);
    $educations = $education_mapper->getEducation();

    $apply_id = 0;
    $apply = [];

    if( $_SESSION['user_id'] ){
        $user_id = (int)$_SESSION['user_id'];
        $mapper = new UserMapper($this->db);
        $user = $mapper->getUserById($user_id);
        $apply_id = $user->getApplyId();
        if( $apply_id != 0 ){
            $apply_mapper = new UserApplyTmpMapper($this->db);
            $apply = $apply_mapper->getApplyById($apply_id);
        }
    }
    
    $response = $this->view->render($response, "jobdetail.phtml", ["apply_id"=> $apply_id, "apply"=>$apply, "categorys" => $categorys,"englishs" => $englishs,"educations" => $educations, "base_url"=>$request->getUri()->getBaseUrl(),"job_detail" => $job[0],"path_url"=>$request->getUri()]);
    return $response;
});
$app->get("/login", function ($request, $response, $args){
    if( $this->session->get('last_url') != '/login'){
        $this->session->set('error_msg', null );
    }
    $categorys_mapper = new CategoryMapper($this->db);
    $categorys = $categorys_mapper->getCategory();
    $this->session->set('last_url', '/login');
    $response = $this->view->render($response, "login.phtml", [ "categorys" => $categorys,"base_url"=>$request->getUri()->getBaseUrl() ]);
    return $response;
});
$app->post("/login", function ($request, $response, $args){
    $this->session->set('last_url', '/login');
    $data = $request->getParsedBody();
    $user_data = [];
    $user_data['email'] = $data['email'];
    $user_data['pwd'] = md5($data['pwd']);
    // print_r($user_data);
    $user = new UserEntity($user_data);
    $user_mapper = new UserMapper($this->db);
    $result = $user_mapper->checkMember($user);
    // print_r($result);
    if( isset($result['error']) ){
        $this->session->set('error_msg', $result);
        $response = $response->withRedirect("/login");
    }else{
        $this->session->set('login', $user_data);
        $this->session->set('verify', $result['user']['authority']);
        $this->session->set('user_id', $result['user']['id']);
        $response = $response->withRedirect("/");
    }
    return $response;
});
$app->get("/logout", function($request, $response){
   $this->session->destroy();
   $response = $response->withRedirect("/login");
   return $response;
});
$app->get("/user/new", function($request, $response, $args){

    if( $this->session->get('last_url') != '/user/new'){
        $this->session->set('error_msg', null );
        $this->session->set('user', null );
    }
    $categorys_mapper = new CategoryMapper($this->db);
    $categorys = $categorys_mapper->getCategory();
    $this->session->set('last_url', '/user/new');
    $response = $this->view->render($response, "usernew.phtml", [ "categorys" => $categorys,"base_url"=>$request->getUri()->getBaseUrl() ]);
    return $response;
});
$app->post("/user/new", function($request, $response, $args){
    $this->session->set('last_url', '/user/new');
    $data = $request->getParsedBody();
    
    $user_data = [];
    $user_data['email'] = $data['email'];
    $user_data['pwd'] = md5($data['pwd']);
    $user_data['name'] = $data['name'];
    $user_data['phone'] = $data['phone'];
    $user_data['people_email'] = ($data['people']=='on')?1:0;
    $user_data['job_email'] = ($data['job']=='on')?1:0;
    $user_data['token'] = generateRandomString(15);
    
    $this->session->set('user', $data);

    $user = new UserEntity($user_data);
    $user_mapper = new UserMapper($this->db);
    $result = $user_mapper->createUser($user);
    // print_r( $result['error'] );
    if( isset($result['error']) ){
        $this->session->set('error_msg', $result['error']);
        // print_r( $_SESSION );
        $response = $response->withRedirect("/user/new");
    }else{
        $send_result = send_mail($user_data);
        $this->session->set('mail_msg',$send_result['msg']);
        // $this->session->set('login', $user_data);
        // $this->session->set('verify', 0);
        // $this->session->set('user_id', $result['user_id']);

        // $this->session->set('test',$result);
        $response = $response->withRedirect("/");
    }
    
    return $response;
});
$app->get("/confirmation/new", function ($request, $response, $args){
    if( $this->session->get('last_url') != '/confirmation/new'){
        $this->session->set('error_msg', null );
    }
    $categorys_mapper = new CategoryMapper($this->db);
    $categorys = $categorys_mapper->getCategory();
    $this->session->set('last_url', '/confirmation/new');
    $response = $this->view->render($response, "confirmation.phtml", [ "categorys" => $categorys,"base_url"=>$request->getUri()->getBaseUrl() ]);
    return $response;
});
$app->post("/confirmation/new", function ($request, $response, $args){
    $this->session->set('last_url', '/confirmation/new');
    $data = $request->getParsedBody();
    $user_data = [];
    $user_data['email'] = $data['email'];

    $user = new UserEntity($user_data);
    $user_mapper = new UserMapper($this->db);
    $result = $user_mapper->checkStatus($user);
    // print_r($result);
    if( isset($result['error']) ){
        $this->session->set('error_msg', $result);
        $response = $response->withRedirect("/confirmation/new");
    }else{
        $this->session->set('mail_msg',$result['msg']);
        $response = $response->withRedirect("/confirmation/new");
    }
    return $response;
});
$app->get("/password/edit", function ($request, $response, $args){
    if( $this->session->get('last_url') != '/password/edit'){
        $this->session->set('error_msg', null );
    }
    $categorys_mapper = new CategoryMapper($this->db);
    $categorys = $categorys_mapper->getCategory();
    $this->session->set('last_url', 'password/edit');

    $params = $request->getQueryParams(); 

    $response = $this->view->render($response, "pwd_edit.phtml", [ "token"=> $params['token'], "categorys" => $categorys,"base_url"=>$request->getUri()->getBaseUrl() ]);
    return $response;
});
$app->post("/password/edit", function ($request, $response, $args){
    $this->session->set('last_url', '/password/edit');
    $data = $request->getParsedBody();
    // print($data);
    $user_data = [];
    if( $data['token']=='' ){
        $this->session->set('error_msg', '此密碼重置無效，請重新申請。');
        $response = $response->withRedirect("/password/edit?token=".$data['token']."&email=".$data['email']);
        return $response; 
    }
    $user_data['email'] = $data['email'];
    $user_data['pwd_token'] = $data['token'];
    $user_data['pwd'] = md5($data['new_pwd']);
    if( $data['new_pwd'] != $data['re_pwd'] ){
        $this->session->set('error_msg', '兩次密碼輸入不同 請檢查');
        $response = $response->withRedirect("/password/edit?token=".$data['token']."&email=".$data['email']);
        return $response; 
    }else{
        $user_data['pwd'] = md5($data['new_pwd']);
    }
    $user = new UserEntity($user_data);
    $user_mapper = new UserMapper($this->db);
    $result = $user_mapper->updatePWD($user);
    // print_r($user_data);
    if( isset($result['error']) ){
        $this->session->set('error_msg',$result['error']);
        $response = $response->withRedirect("/password/edit?token=".$data['token']."&email=".$data['email']);
    }else{
        
        $user = new UserEntity($user_data);
        $user_mapper = new UserMapper($this->db);
        $result_user = $user_mapper->checkMember($user);
        // print_r($result);
        if( isset($result_user['error']) ){
            $this->session->set('error_msg', $result_user['error']);
            $response = $response->withRedirect("/login");
        }else{
            $this->session->set('mail_msg',$result['success']);
            $this->session->set('login', $user_data);
            $this->session->set('verify', $result_user['user']['authority']);
            $this->session->set('user_id', $result_user['user']['id']);
            $response = $response->withRedirect("/");
        }
    }
    return $response;
});
$app->get("/password/new", function ($request, $response, $args){
    if( $this->session->get('last_url') != '/password/new'){
        $this->session->set('error_msg', null );
    }
    $categorys_mapper = new CategoryMapper($this->db);
    $categorys = $categorys_mapper->getCategory();
    $this->session->set('last_url', '/password/new');
    $response = $this->view->render($response, "pwd_new.phtml", [ "categorys" => $categorys,"base_url"=>$request->getUri()->getBaseUrl() ]);
    return $response;
});
$app->post("/password/new", function ($request, $response, $args){
    $this->session->set('last_url', '/password/new');
    $data = $request->getParsedBody();
    $user_data = [];
    $user_data['email'] = $data['email'];

    $user = new UserEntity($user_data);
    $user_mapper = new UserMapper($this->db);
    $result = $user_mapper->pwd_apply($user);
    // print_r($result);
    if( isset($result['error']) ){
        $this->session->set('error_msg', $result);
        $response = $response->withRedirect("/password/new");
    }else{
        $this->session->set('mail_msg',$result['msg']);
        $response = $response->withRedirect("/password/new");
    }
    return $response;
});
$app->get("/user/update", function($request, $response, $args){
    if( !$login = $this->session->get('login') ){
        $response = $response->withRedirect("/login");
        return $response;
    }
    $post_msg = '';
    if( $_SESSION['last_url'] != '/user/update') {
        $this->session->set('error_msg', null);
    }else{
        if( isset($_SESSION['post_msg']) ){
            $post_msg = '會員資料修改成功';
            unset($_SESSION['post_msg']);
        }
    }
    $categorys_mapper = new CategoryMapper($this->db);
    $categorys = $categorys_mapper->getCategory();

    $user_id = (int)$_SESSION['user_id'];
    $mapper = new UserMapper($this->db);
    $user = $mapper->getUserById($user_id);

    $user_data = [];
    $user_data['email'] = $user->getEmail();
    $user_data['name'] = $user->getName();
    $user_data['phone'] = $user->getPhone();
    $user_data['pwd'] = $user->getMd5Pwd();
    $user_data['people'] = $user->getPeopleEmail();
    $user_data['job'] = $user->getJobEmail();
    $this->session->set('login', $user_data);

    $this->session->set('last_url', '/user/update');
    $response = $this->view->render($response, "userupdate.phtml", [ "post_msg"=>$post_msg ,"user"=> $user, "categorys" => $categorys, "base_url"=>$request->getUri()->getBaseUrl() ]);
    return $response;
});
$app->post("/user/update", function($request, $response){
    $this->session->set('error_msg', null);
    $this->session->set('last_url', '/user/update');

    $data = $request->getParsedBody();

    if( md5($data['pwd']) == $_SESSION['login']['pwd'] ){
        $user_data = [];
        $user_data['email'] = $data['email'];
        $user_data['name'] = $data['name'];
        $user_data['phone'] = $data['phone'];
        $user_data['pwd'] = md5($data['pwd']);
        $user_data['id'] = $_SESSION['user_id'];
        $user_data['people_email'] = ($data['people']=='on')?1:0;
        $user_data['job_email'] = ($data['job']=='on')?1:0;


        if( $data['new_pwd'] != ''){
            if( $data['new_pwd'] != $data['re_pwd'] ){
                $this->session->set('error_msg', array('error'=>'兩次密碼輸入不同 請檢查','field'=>'re_pwd'));
                $response = $response->withRedirect("/user/update");
                return $response; 
            }else{
                $user_data['pwd'] = md5($data['new_pwd']);
            }
        }

        $user = new UserEntity($user_data);
        $user_mapper = new UserMapper($this->db);
        $result = $user_mapper->updateUser($user);
        if( isset($result['error']) ){
            $this->session->set('error_msg', $result);
        }else{
            $this->session->set('post_msg', 'success');
            $this->session->set('login', $user_data);
        }
        $response = $response->withRedirect("/user/update");
        return $response; 
    }else{
        $this->session->set('error_msg',array('error'=>'密碼錯誤 請檢查','field'=>'pwd'));
        $response = $response->withRedirect("/user/update");
        return $response; 
    }
});
$app->get("/user/apply/update", function($request, $response, $args){
    if( !$login = $this->session->get('login') ){
        $response = $response->withRedirect("/login");
        return $response;
    }   
    $categorys_mapper = new CategoryMapper($this->db);
    $categorys = $categorys_mapper->getCategory();
    $english_mapper = new EnglishMapper($this->db);
    $englishs = $english_mapper->getEnglish();

    $education_mapper = new EducationMapper($this->db);
    $educations = $education_mapper->getEducation();

    $user_id = (int)$_SESSION['user_id'];
    $mapper = new UserMapper($this->db);
    $user = $mapper->getUserById($user_id);
    // print_r($user);
    $apply_id = $user->getApplyId();
    $apply = [];
    if( $apply_id != 0 ){
        $apply_mapper = new UserApplyTmpMapper($this->db);
        $apply = $apply_mapper->getApplyById($apply_id);
    }else{
        // $apply_data = new UserApplyTmpEntity($user_data);
        $apply_mapper = new UserApplyTmpMapper($this->db);
        $apply_result = $apply_mapper->new_apply($user);
        $apply_id = $apply_result['id'];
        // print('new_apply_id:'.$apply_id);
        $user_mapper = new UserMapper($this->db);
        $user_mapper->updateUserApplyId($user_id, $apply_id);
        $apply = $apply_mapper->getApplyById($apply_id);
        // print_r($apply);
    }

    $this->session->set('last_url', '/user/apply/update');
    $response = $this->view->render($response, "applyupdate.phtml", [ "apply_id"=> $apply_id, "apply"=>$apply, "categorys" => $categorys, "englishs" => $englishs, "educations" => $educations, "base_url"=>$request->getUri()->getBaseUrl() ]);
    return $response;

});
$app->post("/user/apply/update", function($request, $response){
    
    $data = $request->getParsedBody();

    $this->session->set('last_url', '/user/apply/update');
    // print_r($data);
    $data['cv_path_tmp'] = $data['cv_path'];
    $data['cv_path'] = '';
    
    // TODO check file size 
    $files = $request->getUploadedFiles();
    if (!empty($files['newfile'])) {
        $newfile = $files['newfile'];
        // print_r($newfile);
        if ($newfile->getError() === UPLOAD_ERR_OK) {
            $uploadFileName = $newfile->getClientFilename();
            $target_path = generateRandomString().'_'.$uploadFileName;
            $newfile->moveTo("./application/". $target_path );
            $data['cv_path'] = $target_path;
        }
    }
    if( ($data['resume_delete']=='on') || $data['cv_path'] ){
        $file_path = $_SERVER['DOCUMENT_ROOT'] .'/application/'.$data['cv_path_tmp'];
        // print($file_path);
        if(is_file($file_path)){
            unlink($file_path);
        }
    }else{
        $data['cv_path'] = $data['cv_path_tmp'];
    }
    $user_id = (int)$_SESSION['user_id'];
    $user_mapper = new UserMapper($this->db);
    $user = $user_mapper->getUserById($user_id);
    $apply_id = $user->getApplyId();

    if( $apply_id == 0 ){
        $apply = new UserApplyTmpEntity($data);
        $apply_mapper = new UserApplyTmpMapper($this->db);
        $apply_result = $apply_mapper->save($apply);
        $user_apply_id = $apply_result['id'];
        $user_mapper->updateUserApplyId($user_id, $user_apply_id);
    }else{
        $data['id'] = $apply_id;
        $apply = new UserApplyTmpEntity($data);
        $apply_mapper = new UserApplyTmpMapper($this->db);
        $apply_mapper->update($apply);
    }
    $response = $response->withRedirect("/user/apply/update");
    return $response;
});

$app->get('/users/admin',function( $request, $response){
    if( !$login = $this->session->get('login') ){
        $response = $response->withRedirect("/login");
        return $response;
    }
    $categorys_mapper = new CategoryMapper($this->db);
    $categorys = $categorys_mapper->getCategory();

    $params = $request->getQueryParams(); 
    $param_keyword = isset($params['keyword'])? $params['keyword']:'';
    
    $users_query = "SELECT u.id, u.name, u.phone, u.email, u.authority, u.status
                    from users u";
    if( $param_keyword != ''){
        $users_query .= " where (u.name LIKE '%$param_keyword%'
                        OR u.email LIKE '%$param_keyword%' OR u.phone LIKE '%$param_keyword%')"; 
    }
              
    // print($users_query);
    $Paginator  = new Paginator($this->db, $users_query);
    
    $limit = ( isset( $params['limit'] ) ) ? $params['limit'] : 12;
    $page = ( isset( $params['page'] ) ) ? $params['page'] : 1;
    $links = ( isset( $params['links'] ) ) ? $params['links'] : 7;
    $results  = $Paginator->getData( $limit, $page, $links, "UserEntity" );
    $users = $results->data;

    $response = $this->view->render($response, "users_admin.phtml", ["categorys" => $categorys, "result"=>$results,"users"=>$users, "Paginator"=> $Paginator, "path"=>$request->getUri()->getPath(),"url"=>$request->getUri()->getQuery(),"base_url"=>$request->getUri()->getBaseUrl() ]);
    return $response;
});
$app->get('/user/apply/history',function( $request, $response){
    if( !$login = $this->session->get('login') ){
        $response = $response->withRedirect("/login");
        return $response;
    }
    $categorys_mapper = new CategoryMapper($this->db);
    $categorys = $categorys_mapper->getCategory();

    $english_mapper = new EnglishMapper($this->db);
    $education_mapper = new EducationMapper($this->db);

    $applications = [];
    if( $user_id = $this->session->get('user_id') ){
        $application_mapper = new ApplicationMapper($this->db);
        $applications = $application_mapper->getApplicationByUser($user_id);
    }    
    $response = $this->view->render($response, "my_application.phtml", ["english_mapper"=>$english_mapper,"education_mapper"=>$education_mapper, "applications"=> $applications,"categorys" => $categorys,"path"=>$request->getUri()->getPath(),"url"=>$request->getUri()->getQuery(),"base_url"=>$request->getUri()->getBaseUrl() ]);
    return $response;
});
$app->get('/confirmation', function ($request, $response) {
    $params = $request->getQueryParams(); 
    // print_r($data);
    $user = new UserEntity($params);
    $user_mapper = new UserMapper($this->db);
    $re = $user_mapper->confirm($user);
    $this->session->set('mail_msg',$re['msg']);
    return $response->withRedirect("/");
});   

$app->post('/user/status/update', function ($request, $response, $args) {
    $data = $request->getParsedBody();
    // print( $data['id'] ); 
    $user_data = [];
    $user_data['id'] = $data['id'];
    $user_data['status'] = $data['status'];
    // $user_data['authority'] = $data['authority'];

    $user = new UserEntity($user_data);
    $user_mapper = new UserMapper($this->db);
    $re = $user_mapper->update_status($user);

    return $response->withJson($re);
});
$app->post('/user/authority/update', function ($request, $response, $args) {
    $data = $request->getParsedBody();
    // print( $data['id'] ); 
    $user_data = [];
    $user_data['id'] = $data['id'];
    $user_data['authority'] = $data['authority'];
    // $user_data['authority'] = $data['authority'];

    $user = new UserEntity($user_data);
    $user_mapper = new UserMapper($this->db);
    $re = $user_mapper->update_authority($user);

    return $response->withJson($re);
});

$app->post('/apply/check', function ($request, $response, $args) {
    $data = $request->getParsedBody();
    $sql = "SELECT * FROM `application` WHERE `job_id`=".$data['job_id']." and `apply_user_id`=".$data['apply_user_id'];
    $stmt = $this->db->query($sql);
    
    if( $stmt->rowCount() == 0 ){
        return $response->withJson(array('success'=>'未申請過此工作'));
    }else{
        return $response->withJson(array('error'=>'您有申請過該工作，請問確定再次寄出職缺申請嗎？'));
    }     

});
$app->post('/mail', function ($request, $response, $args) {
    $data = $request->getParsedBody();
    send_mail($data);
    return $request->withJson($data);
});
function generateRandomString($length = 5) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}
function encoding($msg) {
    return mb_encode_mimeheader($msg, 'UTF-8', 'B', "\r\n", strlen('Subject: '));
}

function send_pwd_mail($email,$pwd_token){
    mb_internal_encoding('UTF-8');

    $mail = new PHPMailer;
    $mail->CharSet = 'utf-8';
    ini_set('default_charset', 'UTF-8');

    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'mail.iis.sinica.edu.tw';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'joanne3634';                 // SMTP username
    $mail->Password = 'Lulu0525';                           // SMTP password
    $mail->SMTPSecure = 'tls';                     // Enable encryption, 'ssl' also accepted
    $mail->IsHTML(true);

    // $mail->addReplyTo( $user_data['email'], $user_data['name'] );

    $mail->From = 'no-reply@datasci.tw';
    $mail->FromName = '資料科學人才媒合平台';

    $mail->addAddress($email);     // Add a recipient
    $mail->addBCC('b00902007@ntu.edu.tw', encoding('CC信箱'));     // Add a recipient
    $mail->WordWrap = 50;                                 // Set word wrap to 50 characters
    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = encoding('[資料科學人才媒合平台] 重設密碼');
    $mail->Body = '   
    <div style="line-height: 20px;">
    您好，'.$email.'！<br>
    欲變更密碼請點擊下方連結： <br><br>
   
    <a target="_blank" href="http://jobs.datasci.tw/password/edit?token='.$pwd_token.'&email='.$email.'">我要變更密碼</a><br>
    若您未曾申請變更密碼，請直接捨棄此封郵件。<br>
在您點擊上方[我要變更密碼]連結以前，<br>
您的密碼都不會更改，敬請安心。<br>
    ******************<br><br>
    本徵才服務由<a target="_blank" href="http://foundation.datasci.tw/">台灣資料科學協會</a>提供。如有任何問題或建議，歡迎隨時來信與我們聯絡： <a href="mailto:secretary@datasci.tw" target="_top">secretary@datasci.tw</a></div>

    ';
    if(!$mail->send()) {
        return array('status'=>'error','msg'=> $mail->ErrorInfo) ;
        // return false;
    } else {
        return array('status'=>'success','msg'=> '您將在幾分鐘後收到一封電子郵件，內有重新設定密碼的步驟說明。');
        // return true;
    }
}
function send_mail($user_data){
    mb_internal_encoding('UTF-8');

    $mail = new PHPMailer;
    $mail->CharSet = 'utf-8';
    ini_set('default_charset', 'UTF-8');

    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'mail.iis.sinica.edu.tw';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'joanne3634';                 // SMTP username
    $mail->Password = 'Lulu0525';                           // SMTP password
    $mail->SMTPSecure = 'tls';                     // Enable encryption, 'ssl' also accepted
    $mail->IsHTML(true);

    // $mail->addReplyTo( $user_data['email'], $user_data['name'] );

    $mail->From = 'no-reply@datasci.tw';
    $mail->FromName = '資料科學人才媒合平台';

    $mail->addAddress($user_data['email'], $user_data['name']);     // Add a recipient
    $mail->addBCC('b00902007@ntu.edu.tw', encoding('CC信箱'));     // Add a recipient
    $mail->WordWrap = 50;                                 // Set word wrap to 50 characters
    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = encoding('[資料科學人才媒合平台] 請確認您的電子郵件帳號');
    $mail->Body = '   
    <div style="line-height: 20px;">
    您好，'.$user_data['name'].'！<br>
    歡迎您登錄「資料科學人才媒合平台」。 <br><br>
    您使用 '.$user_data['email'].' 註冊，<br>
    若確認無誤請點擊下方連結進行會員認證。 <br><br>
    <a target="_blank" href="http://jobs.datasci.tw/confirmation?token='.$user_data['token'].'&email='.$user_data['email'].'">我要認證</a><br>
    ******************<br><br>
    本徵才服務由<a target="_blank" href="http://foundation.datasci.tw/">台灣資料科學協會</a>提供。如有任何問題或建議，歡迎隨時來信與我們聯絡： <a href="mailto:secretary@datasci.tw" target="_top">secretary@datasci.tw</a></div>

    ';

    if(!$mail->send()) {
        return array('status'=>'error','msg'=> $mail->ErrorInfo) ;
        // return false;
    } else {
        return array('status'=>'success','msg'=> '確認信件將在幾分鐘後送至您的 Email 信箱');
        // return true;
    }
}

function send_apply_to_job_owner($apply_data,$owner_data,$english_opt,$educations_opt){
   
    mb_internal_encoding('UTF-8');

    $mail = new PHPMailer;
    $mail->CharSet = 'utf-8';
    ini_set('default_charset', 'UTF-8');

    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'mail.iis.sinica.edu.tw';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'joanne3634';                 // SMTP username
    $mail->Password = 'Lulu0525';                           // SMTP password
    $mail->SMTPSecure = 'tls';                     // Enable encryption, 'ssl' also accepted
    $mail->IsHTML(true);

    // $mail->addReplyTo( $user_data['email'], $user_data['name'] );

    $mail->From = 'no-reply@datasci.tw';
    $mail->FromName = '資料科學人才媒合平台';

    $mail->addAddress($owner_data['email'], $owner_data['name']);     // Add a recipient
    $mail->addBCC('b00902007@ntu.edu.tw', encoding('CC信箱'));     // Add a recipient
    $mail->WordWrap = 50;                                 // Set word wrap to 50 characters
    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = encoding('[資料科學人才媒合平台] 投遞履歷');
    $mail->Body = '   
    <div style="line-height: 20px;">
    
您好，'.$apply_data['name'].' 透過 資料科學人才媒合平台 向貴單位投遞履歷，以下是他/她的履歷資料。<br><br>

姓名：'.$apply_data['name'].'<br>
信箱：'.$apply_data['email'].'<br>
電話：'.$apply_data['phone'].'<br>
出生年份：'.$apply_data['age_year'].'<br>
畢業年份：'.$apply_data['graduate_year'].'<br>
畢業學校：'.$apply_data['gradute_school'].'<br>
畢業科系：'.$apply_data['gradute_master'].'<br>
目前或最後一間公司名稱：'.$apply_data['latest_company'].'<br>
目前或最後一份工作職稱：'.$apply_data['latest_job_title'].'<br>
英文能力：'.$apply_data['english_ability_id'].' '.$english_opt.'<br>
最高學歷：'.$apply_data['highest_education_id'].' '.$educations_opt.'<br>
處理資料的相關能力經驗：<br>

'.$apply_data['experience'].'<br>
其他能力、經驗：<br>
'.$apply_data['other'].'<br>

    ******************<br><br>
    本徵才服務由<a target="_blank" href="http://foundation.datasci.tw/">台灣資料科學協會</a>提供。如有任何問題或建議，歡迎隨時來信與我們聯絡： <a href="mailto:secretary@datasci.tw" target="_top">secretary@datasci.tw</a></div>

    ';
    
    $file_path = $_SERVER['DOCUMENT_ROOT'] .'/application/'.$apply_data['cv_path'];    
    if(is_file($file_path)){
        $mail->AddAttachment($file_path);
    }
    
    if(!$mail->send()) {
        return array('status'=>'error','msg'=> $mail->ErrorInfo) ;
        // return false;
    } else {
        return array('status'=>'success','msg'=> '確認信件將在幾分鐘後送至您的 Email 信箱');
        // return true;
    }
}

function square_thumbnail_with_proportion($src_file,$destination_file,$square_dimensions,$jpeg_quality=90)
{
    // Step one: Rezise with proportion the src_file *** I found this in many places.
    $src_img = imagecreatefromstring(file_get_contents($src_file));
    $image = imagecreatetruecolor(imagesx($src_img), imagesy($src_img));
    $white = imagecolorallocate($image, 255, 255, 255);
    imagefill($image, 0, 0, $white);
    imagecopy($image, $src_img, 0, 0, 0, 0, imagesx($image), imagesy($image));
    // $src_img=imagecreatefromjpeg($src_file);

    $old_x=imageSX($image);
    $old_y=imageSY($image);

    $ratio1=$old_x/$square_dimensions;
    $ratio2=$old_y/$square_dimensions;

    if($ratio1>$ratio2)
    {
        $thumb_w=$square_dimensions;
        $thumb_h=$old_y/$ratio1;
    }
    else    
    {
        $thumb_h=$square_dimensions;
        $thumb_w=$old_x/$ratio2;
    }

    // we create a new image with the new dimmensions
    $smaller_image_with_proportions=ImageCreateTrueColor($thumb_w,$thumb_h);

    // resize the big image to the new created one
    imagecopyresampled($smaller_image_with_proportions,$image,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y); 

    // *** End of Step one ***

    // Step Two (this is new): "Copy and Paste" the $smaller_image_with_proportions in the center of a white image of the desired square dimensions

    // Create image of $square_dimensions x $square_dimensions in white color (white background)
    $final_image = imagecreatetruecolor($square_dimensions, $square_dimensions);
    $bg = imagecolorallocate ( $final_image, 255, 255, 255 );
    // imagefilledrectangle($final_image,0,0,$square_dimensions,$square_dimensions,$bg);
    imagefill($final_image,0,0,$bg);
    // need to center the small image in the squared new white image
    if($thumb_w>$thumb_h)
    {
        // more width than height we have to center height
        $dst_x=0;
        $dst_y=($square_dimensions-$thumb_h)/2;
    }
    elseif($thumb_h>$thumb_w)
    {
        // more height than width we have to center width
        $dst_x=($square_dimensions-$thumb_w)/2;
        $dst_y=0;

    }
    else
    {
        $dst_x=0;
        $dst_y=0;
    }

    $src_x=0; // we copy the src image complete
    $src_y=0; // we copy the src image complete

    $src_w=$thumb_w; // we copy the src image complete
    $src_h=$thumb_h; // we copy the src image complete

    $pct=100; // 100% over the white color ... here you can use transparency. 100 is no transparency.

    imagecopymerge($final_image,$smaller_image_with_proportions,$dst_x,$dst_y,$src_x,$src_y,$src_w,$src_h,$pct);

    imagejpeg($final_image,$destination_file,$jpeg_quality);

    // destroy aux images (free memory)
    imagedestroy($image); 
    imagedestroy($smaller_image_with_proportions);
    imagedestroy($final_image);
}