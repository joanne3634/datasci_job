<?php

class UserApplyTmpMapper extends Mapper
{   
    public function getApplyById($apply_id) {
        $sql = "SELECT * from users_application where id = :id";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute(["id" => $apply_id]);

        if($result) {
            return new UserApplyTmpEntity($stmt->fetch());
        }
    }
    public function new_apply(UserEntity $application) {
        $sql = "insert into users_application
            (email,name,phone,english_ability_id, highest_education_id,graduate_year,age_year) values
            (:email,:name,:phone,:english_ability_id, :highest_education_id,:graduate_year,:age_year)";

        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([
            "email"=> $application->getEmail(),
            "name"=>$application->getName(),
            "phone"=>$application->getPhone(),
            "english_ability_id" => 0,
            "highest_education_id" => 0,
            "graduate_year" => 0,
            "age_year" => 0
        ]);

        if(!$result) {
            throw new Exception("could not new record");
        }else{
            $id =  $this->db->lastInsertId();
            return array('success'=>'成功新增履歷','id'=>$id);
        }
    }
    public function save(UserApplyTmpEntity $application) {
        $sql = "insert into users_application
            (name, email, phone, latest_company, latest_job_title, gradute_school, gradute_master, experience, english_ability_id, other,highest_education_id,graduate_year,age_year,cv_path) values
            (:name, :email, :phone, :latest_company, :latest_job_title, :gradute_school, :gradute_master, :experience, :english_ability_id, :other,:highest_education_id,:graduate_year,:age_year,:cv_path)";

        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([
            "name" => $application->getName(),
            "email" => $application->getEmail(),
            "phone" => $application->getPhone(),
            "latest_company" => $application->get_latest_company(),
            "latest_job_title" => $application->get_latest_job_title(),
            "gradute_school" => $application->get_gradute_school(),
            "gradute_master" => $application->get_gradute_master(),
            "experience" => $application->get_experience(),
            "english_ability_id" => $application->get_english_ability_id(),
            "other" => $application->getOther(),
            "highest_education_id" => $application->get_highest_education_id(),
            "graduate_year" => $application->get_graduate_year(),
            "age_year" => $application->get_age_year(),
            "cv_path" => $application->getCVpath()
        ]);

        if(!$result) {
            throw new Exception("could not save record");
        }else{
            $id =  $this->db->lastInsertId();
            return array('success'=>'成功新增履歷','id'=>$id);
        }
    }
    public function update(UserApplyTmpEntity $application) {
        $sql = "update users_application set
            name = :name, email = :email, phone= :phone,latest_company= :latest_company,latest_job_title= :latest_job_title,gradute_school= :gradute_school,gradute_master= :gradute_master,experience= :experience,english_ability_id= :english_ability_id,other= :other,highest_education_id=:highest_education_id,graduate_year=:graduate_year,age_year=:age_year,cv_path=:cv_path where id=".$application->getId();

        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([
            "name" => $application->getName(),
            "email" => $application->getEmail(),
            "phone" => $application->getPhone(),
            "latest_company" => $application->get_latest_company(),
            "latest_job_title" => $application->get_latest_job_title(),
            "gradute_school" => $application->get_gradute_school(),
            "gradute_master" => $application->get_gradute_master(),
            "experience" => $application->get_experience(),
            "english_ability_id" => $application->get_english_ability_id(),
            "other" => $application->getOther(),
            "highest_education_id" => $application->get_highest_education_id(),
            "graduate_year" => $application->get_graduate_year(),
            "age_year" => $application->get_age_year(),
            "cv_path" => $application->getCVpath()
        ]);

        if(!$result) {
            throw new Exception("could not update record");
        }
    }
}