<?php

class EducationEntity
{
    protected $id;
    protected $highest_education;

    public function __construct(array $data) {
        $this->id = $data['id'];
        $this->highest_education = $data['highest_education'];
    }

    public function getId() {
        return $this->id;
    }

    public function getEducation() {
        return $this->highest_education;
    }
}
