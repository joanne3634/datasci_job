<?php

class UserApplyTmpEntity
{
    protected $id;
    protected $name;
    protected $email;
    protected $phone;
    protected $latest_company;
    protected $latest_job_title;
    protected $gradute_school;
    protected $gradute_master;
    protected $experience;
    protected $english_ability_id;
    protected $other;
    protected $highest_education_id;
    protected $graduate_year;
    protected $age_year;
    protected $cv_path;

    public function __construct(array $data) {
        if(isset($data['id'])) {
            $this->id = $data['id'];
        }
        if(isset($data['latest_company'])){
            $this->latest_company = $data['latest_company'];
            $this->latest_job_title = $data['latest_job_title'];
            $this->gradute_school = $data['gradute_school'];
            $this->gradute_master = $data['gradute_master'];
            $this->experience = $data['experience'];
            $this->english_ability_id = $data['english_ability_id'];
            $this->other = $data['other'];
        }
        $this->name = $data['name'];
        $this->email = $data['email'];
        $this->phone = $data['phone'];

        if(isset($data['highest_education_id'])) {
            $this->highest_education_id = $data['highest_education_id'];
            $this->graduate_year = $data['graduate_year'];
            $this->age_year = $data['age_year'];
        }
        if(isset($data['cv_path'])) {
            $this->cv_path = $data['cv_path'];
        }
    }
    public function getId() {
        return $this->id;
    }
    public function getName() {
        return $this->name;
    }
    public function getEmail() {
        return $this->email;
    }
    public function getPhone() {
        return $this->phone;
    }
    public function get_latest_company() {
        return $this->latest_company;
    }
    public function get_latest_job_title() {
        return $this->latest_job_title;
    }
    public function get_gradute_school() {
        return $this->gradute_school;
    }
    public function get_gradute_master() {
        return $this->gradute_master;
    }
    public function get_experience() {
        return $this->experience;
    }
    public function get_english_ability_id() {
        return $this->english_ability_id;
    }
    public function get_highest_education_id() {
        return $this->highest_education_id;
    }
    public function get_graduate_year() {
        return $this->graduate_year;
    }
    public function get_age_year() {
        return $this->age_year;
    }
    public function getOther() {
        return $this->other;
    }
    public function getCVpath() {
        return $this->cv_path;
    }
}