<?php

class JobTypeEntity
{
    protected $id;
    protected $job_type;

    public function __construct(array $data) {
        $this->id = $data['id'];
        $this->job_type = $data['job_type'];
    }

    public function getId() {
        return $this->id;
    }

    public function getJobType() {
        return $this->job_type;
    }
}
