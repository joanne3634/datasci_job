<?php

class CategoryEntity
{
    protected $id;
    protected $category;

    public function __construct(array $data) {
        $this->id = $data['id'];
        $this->category = $data['category'];
    }

    public function getId() {
        return $this->id;
    }

    public function getCategory() {
        return $this->category;
    }
}
