<?php

class EnglishMapper extends Mapper
{
    public function getEnglish() {
        $sql = "SELECT id, english
            from english";
        $stmt = $this->db->query($sql);

        $results = [];
        while($row = $stmt->fetch()) {
            $results[] = new EnglishEntity($row);
        }
        return $results;
    }

    public function getEnglishById($id) {
        $sql = "SELECT id, english
            from english where id = :english_id";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(["english_id" => $id]);

        return $stmt->fetch();
    }
}

