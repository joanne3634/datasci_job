<?php

class CategoryMapper extends Mapper
{
    public function getCategory() {
        $sql = "SELECT id, category
            from category";
        $stmt = $this->db->query($sql);

        $results = [];
        while($row = $stmt->fetch()) {
            $results[] = new CategoryEntity($row);
        }
        return $results;
    }

    public function getCategoryById($id) {
        $sql = "SELECT id, category
            from category where id = :category_id";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(["category_id" => $id]);

        return new CategoryEntity($stmt->fetch());
    }
}

