<?php

class UserMapper extends Mapper
{
    public function getUserByEmail($email) {
        $sql = "SELECT u.id, u.email, u.pwd 
            from users u
            where u.email = :email";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute(["email" => $email]);

        if($result) {
            return new UserEntity($stmt->fetch());
        }
    }

    public function getUserById($user_id) {
        $sql = "SELECT u.id, u.name, u.phone, u.email, u.pwd, u.user_apply_id, u.people_email, u.job_email
            from users u
            where u.id = :id";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute(["id" => $user_id]);

        if($result) {
            return new UserEntity($stmt->fetch());
        }
    }

    public function checkMember(UserEntity $user){
        $sql = "select * from users where `email`= '".$user->getEmail()."'";
        // print($sql);
        $stmt = $this->db->query($sql);
        
        if( $stmt->rowCount() == 0 ){
            return array('error'=>'無此使用者','field'=>'email');
        }    
        $sql = "select * from users where `email`= '".$user->getEmail()."' and `pwd`= '". $user->getMd5Pwd()."'";
        $stmt = $this->db->query($sql);
        
        if( $result = $stmt->fetchAll() ){
            // print_r($result );
            if( !$result[0]['status']){
                return array('error'=>'帳號已停權，如需重新啟用請聯絡管理者：secretary@datasci.tw。','field'=>'email');
            }
            return array('success'=>'登入成功','user'=>$result[0]);
        }else{
            return array('error'=>'密碼錯誤','field'=>'pwd');
        }        
    }
    public function confirm(UserEntity $user){
        $sql = "select * from users where `email`= '".$user->getEmail()."' and `token`= '".$user->getToken()."'";
        // print($sql);
        $stmt = $this->db->query($sql);
        
        if( $stmt->rowCount() == 0 ){
            return array('msg'=>'此認證碼無效');
        }else{
            $user_data = [];
            $authority = 0;
            $id = 0;
            if( $result = $stmt->fetchAll() ){
                $user_data['email'] = $result[0]['email'];
                $user_data['pwd'] = $result[0]['pwd']; 
                $authority = $result[0]['authority']; 
                $id = $result[0]['id'];
            } 
            
            $sql_t = "update users set
                status = :status where `email`= '".$user->getEmail()."'";

            $stmt_t = $this->db->prepare($sql_t);
            $result_t = $stmt_t->execute([
                "status" => '1'
            ]);
                
            if(!$result_t) {
                return array('msg'=>'您的電子郵件地址認證失敗');
            }else{
                $_SESSION['login'] = $user_data;
                $_SESSION['verify'] = $authority;
                $_SESSION['user_id'] = $id;
                return array('msg'=>'您的電子郵件地址已成功確認您的帳號已經過確認。');
            }
        }
    }
    public function checkStatus(UserEntity $user){
        $sql = "select * from users where `email`= '".$user->getEmail()."'";
        $stmt = $this->db->query($sql);
        
        if( $stmt->rowCount() == 0 ){
            return array('error'=>'無此使用者','field'=>'email');
        }    
        
        if( $result = $stmt->fetchAll() ){
            // print_r($result );
            if( $result[0]['status']){
                return array('error'=>'此帳號已驗證','field'=>'email');
            }else{
                return send_mail($result[0]);
            }
            
        }    
    }
    public function pwd_apply(UserEntity $user){
        $sql = "select * from users where `email`= '".$user->getEmail()."'";
        $stmt = $this->db->query($sql);
        
        if( $stmt->rowCount() == 0 ){
            return array('error'=>'無此使用者','field'=>'email');
        }    
       
        $sql_t = "update users set pwd_token = :pwd_token where `email`= '".$user->getEmail()."'";

        $stmt_t = $this->db->prepare($sql_t);
        $pwd_token = generateRandomString(15);
        $result_t = $stmt_t->execute([
            "pwd_token" => $pwd_token
        ]);
        return send_pwd_mail($user->getEmail(),$pwd_token);
   
    }
    public function updatePWD(UserEntity $user){
        $sql = "select * from users where `pwd_token`= '".$user->getpwdToken()."' and `email`= '".$user->getEmail()."'";
        // print($sql);
        $stmt = $this->db->query($sql);
        
        if( $stmt->rowCount() == 0 ){
            return array('error'=>'此密碼重置無效，請重新申請。','field'=>'email');
        }    
        
        $sql_t = "update users set pwd = :pwd, pwd_token =:pwd_token where `pwd_token`='".$user->getpwdToken()."'";
        // print($sql_t);
        // print($user->getMd5Pwd());
        $pwd = $user->getMd5Pwd();
        $stmt_t = $this->db->prepare($sql_t);
        $result_t = $stmt_t->execute([
            "pwd" => $pwd,
            "pwd_token" => ''
        ]);

        if(!$result_t) {
            throw array('error'=>'重置密碼失敗，請重新設置。','field'=>'email');
        }else{
            // $id =  $this->db->lastInsertId();
            return array('success'=>'成功修改密碼，請登入。');
        }
            
        
    }
    public function createUser(UserEntity $user) {

        $sql = "select * from users where `email`= '".$user->getEmail()."'";
        $stmt = $this->db->query($sql);
        
        if( !$stmt->rowCount() ){
            $sql = "insert into users
                (email, pwd, name,phone,people_email,job_email,token) values
                (:email, :pwd, :name,:phone,:people_email,:job_email,:token)";

            $stmt = $this->db->prepare($sql);
            $result = $stmt->execute([
                "email" => $user->getEmail(),
                "pwd" => $user->getMd5Pwd(),
                "name" => $user->getName(),
                "phone" => $user->getPhone(),
                "people_email" => $user->getPeopleEmail(),
                "job_email" => $user->getJobEmail(),
                "token" => $user->getToken()
            ]);    

            // print_r($result );
            if(!$result) {
                throw new Exception("could not createUser");
            }else{
                $id =  $this->db->lastInsertId();
                return array('success'=>'成功註冊，請收取認證信並啟用您的帳戶','user_id'=>$id);
            }
        }else{
            return array('error'=>'信箱已經被使用','field'=>'email');
        }        
    }
    public function updateUser(UserEntity $user) {
        $sql = "select * from users where `id`!= ".$user->getId()." and `email`= '".$user->getEmail()."'";
        // print($sql);
        $stmt = $this->db->query($sql);
        
        if( !$stmt->rowCount() ){
            $sql = "update users set
                email = :email, pwd = :pwd, name= :name,phone= :phone,people_email=:people_email,job_email=:job_email where id=".$user->getId();

            $stmt = $this->db->prepare($sql);
            $result = $stmt->execute([
                "email" => $user->getEmail(),
                "pwd" => $user->getMd5Pwd(),
                "name" => $user->getName(),
                "phone" => $user->getPhone(),
                "people_email" => $user->getPeopleEmail(),
                "job_email" => $user->getJobEmail()
            ]);

            if(!$result) {
                throw new Exception("could not updateUser");
            }else{
                // $id =  $this->db->lastInsertId();
                return array('success'=>'成功修改');
            }
        }else{
            return array('error'=>'信箱已經被使用','field'=>'email');
        }  
    }
    public function updateUserApplyId($user_id, $user_apply_id) {
        $sql = "update users set
            user_apply_id = :user_apply_id where id=".$user_id;

        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([
            "user_apply_id" => $user_apply_id
        ]);

        if(!$result) {
            throw new Exception("could not updateUser");
        }else{
            // $id =  $this->db->lastInsertId();
            return array('success'=>'成功修改');
        }
    }
    public function update_status(UserEntity $user) {
        $sql = "update users set status = :status where id= :user_id ";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([
            "status" => $user->getStatus(),
            "user_id" => $user->getId()
        ]);

        if(!$result) {
            throw new Exception("could not updateUser");
        }else{
            return array('success'=>'成功修改');
        }
    }
    public function update_authority(UserEntity $user) {
        $sql = "update users set authority = :authority where id= :user_id ";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([
            "authority" => $user->getAuthority(),
            "user_id" => $user->getId()
        ]);

        if(!$result) {
            throw new Exception("could not updateUser");
        }else{
            return array('success'=>'成功修改');
        }
    }
}
