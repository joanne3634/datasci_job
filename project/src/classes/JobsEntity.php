<?php

class JobsEntity
{
    protected $id;
    protected $job_title;
    protected $company;
    protected $place;
    protected $job_type;
    protected $experience;
    protected $category;
    protected $job_description;
    protected $website;
    protected $salary_mon_min;
    protected $salary_mon_max;
    // protected $salary_year_max;
    protected $owener_id;   
    protected $visible;
    protected $verify;
    protected $job_type_id;
    protected $experience_id;
    protected $category_id;
    protected $date_mod;

    protected $date_expired;
    protected $date_post;

    protected $owener_name;
    protected $owener_email;

    protected $logo_path;
    protected $company_depart;

    public function __construct(array $data) {
        if(isset($data['id'])) {
            $this->id = $data['id'];
        }
        if(isset($data['owener_id'])) {
            $this->owener_id = $data['owener_id'];
        }
        if(isset($data['name'])) {
            $this->owener_name = $data['name'];
            $this->owener_email = $data['email'];
        }
        if(isset($data['visible'])) {
            $this->visible = $data['visible'];
        }
        if(isset($data['verify'])) {
            $this->verify = $data['verify'];
        }
        if(isset($data['date_mod'])) {
            $this->date_mod = $data['date_mod'];
        }
        if(isset($data['website'])) {
            $this->job_description = $data['job_description'];
            $this->website = $data['website'];
        }
        if(isset($data['experience'])){
            $this->category = $data['category'];
            $this->experience = $data['experience'];
            $this->salary_mon_min = $data['salary_mon_min'];
            $this->salary_mon_max = $data['salary_mon_max'];
            // $this->salary_year_max = $data['salary_year_max'];
        }
        if(isset($data['job_type_id'])){
            $this->job_type_id = $data['job_type_id'];
            $this->experience_id = $data['experience_id'];
            $this->category_id = $data['category_id'];
        }
        if(isset($data['job_title'])){
            $this->job_title = $data['job_title'];
            $this->company = $data['company'];
            $this->place = $data['place'];
            $this->job_type = $data['job_type'];
        }        
        if(isset($data['date_expired'])){
            $this->date_expired = $data['date_expired'];
        }
        if(isset($data['date_post'])){
            $this->date_post = $data['date_post'];
        }
        if(isset($data['logo_path'])){
            $this->logo_path = $data['logo_path'];
        }
        if(isset($data['company_depart'])){
            $this->company_depart = $data['company_depart'];
        }
    }

    public function getId() {
        return $this->id;
    }
    public function getJobTitle() {
        return $this->job_title;
    }
    public function getCompany() {
        return $this->company;
    }
    public function getPlace() {
        return $this->place;
    }
    public function getJobType() {
        return $this->job_type;
    }
    public function getExperience() {
        return $this->experience;
    }
    public function getCategory() {
        return $this->category;
    }
    public function getJobDescription() {
        return $this->job_description;
    }
    public function getWebsite() {
        return $this->website;
    }
    public function get_salary_mon_min() {
        return $this->salary_mon_min;
    }
    public function get_salary_mon_max() {
        return $this->salary_mon_max;
    }
    public function get_owener_id() {
        return $this->owener_id;
    }
    public function get_owener_name() {
        return $this->owener_name;
    }
    public function get_owener_email() {
        return $this->owener_email;
    }

    public function getJobTypeId() {
        return $this->job_type_id;
    }
    public function getExperienceId() {
        return $this->experience_id;
    }
    public function getCategoryId() {
        return $this->category_id;
    }
    public function getVisible() {
        return $this->visible;
    }
    public function getVerify() {
        return $this->verify;
    }
    public function applyCount(){
        $sql = "SELECT count(*) FROM application WHERE job_id = ".$this->id;
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute();
        return $result;
    }
    public function getDateMod(){
        return $this->date_mod;
    }
    public function getDatePost(){
        return $this->date_post;
    }
    public function getDateExpired(){
        // $date=date_create($this->date_expired);
        return $this->date_expired;
        // return date_format($date,"Y/m/d");
    }
    public function getLogoPath(){
        return $this->logo_path;
    }
    public function getCompanyDepart(){
        return $this->company_depart;
    }

}