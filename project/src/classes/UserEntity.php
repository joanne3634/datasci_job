<?php

class UserEntity
{
    protected $id;
    protected $email;
    protected $pwd;
    protected $name;
    protected $phone;
    protected $user_apply_id;
    protected $authority;
    protected $status;
    protected $regis_date;
    protected $mod_date;
    protected $people_email;
    protected $job_email;
    protected $token;
    protected $pwd_token;

    /**
     * Accept an array of data matching properties of this class
     * and create the class
     *
     * @param array $data The data to use to create
     */
    public function __construct(array $data) {
        // no id if we're creating
        if( isset($data['id'])) {
            $this->id = $data['id'];
        }
        if( isset($data['user_apply_id'])) {
            $this->user_apply_id = $data['user_apply_id'];
        }
        if( isset($data['authority'])) {
            $this->authority = $data['authority'];
        }
        if( isset($data['status'])) {
            $this->status = $data['status'];
        }
        if( isset($data['name'])){
            $this->name = $data['name'];
        }
        if( isset($data['people_email'])) {
            $this->people_email = $data['people_email'];
        }
        if( isset($data['job_email'])){
            $this->job_email = $data['job_email'];
        }
         if( isset($data['phone'])){
            $this->phone = $data['phone']; 
        }
        if( isset($data['regis_date'])){
            $this->regis_date = $data['regis_date'];
            $this->mod_date = $data['mod_date'];
        }               
         if( isset($data['email'])){
            $this->email = $data['email']; 
        }
       
        if( isset($data['pwd'])){
            $this->pwd = $data['pwd'];
        }  
        if( isset($data['token'])){
            $this->token = $data['token'];
        }
        if( isset($data['pwd_token'])){
            $this->pwd_token = $data['pwd_token'];
        }  
          
    }

    public function getId() {
        return $this->id;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getMd5Pwd() {
        return $this->pwd;
    }

    public function getName() {
        return $this->name;
    }

    public function getPhone() {
        return $this->phone;
    }
    public function getApplyId() {
        return $this->user_apply_id;
    }
    public function getAuthority() {
        return $this->authority;
    }
    public function getStatus() {
        return $this->status;
    }
    public function getPeopleEmail() {
        return $this->people_email;
    }
    public function getJobEmail() {
        return $this->job_email;
    }
    public function getToken(){
        return $this->token;
    }
    public function getpwdToken(){
        return $this->pwd_token;
    }
    
}
