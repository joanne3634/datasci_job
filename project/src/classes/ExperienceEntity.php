<?php

class ExperienceEntity
{
    protected $id;
    protected $experience;

    public function __construct(array $data) {
        $this->id = $data['id'];
        $this->experience = $data['experience'];
    }

    public function getId() {
        return $this->id;
    }

    public function getExperience() {
        return $this->experience;
    }
}
