<?php

class EnglishEntity
{
    protected $id;
    protected $english;

    public function __construct(array $data) {
        $this->id = $data['id'];
        $this->english = $data['english'];
    }

    public function getId() {
        return $this->id;
    }

    public function getEnglish() {
        return $this->english;
    }
}
