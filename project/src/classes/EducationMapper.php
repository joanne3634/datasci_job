<?php

class EducationMapper extends Mapper
{
    public function getEducation() {
        $sql = "SELECT id, highest_education
            from highest_education ORDER BY `highest_education`.`id` DESC";
        $stmt = $this->db->query($sql);

        $results = [];
        while($row = $stmt->fetch()) {
            $results[] = new EducationEntity($row);
        }
        return $results;
    }

    public function getEducationById($id) {
        $sql = "SELECT id, highest_education
            from highest_education where id = :id";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(["id" => $id]);

        return $stmt->fetch();
    }
}

