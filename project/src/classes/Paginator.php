<?php
 
class Paginator{
 
    private $_limit;
    private $_page;
    private $_query;
    private $_total;
    private $_links;
    private $db;
    
    public function __construct( $db, $query ) {     

	    $this->_query = $query;
	    $this->db = $db;

	    $stmt = $this->db->query( $this->_query );
	    $this->_total = $stmt->rowCount();
	     
	}
	public function getData( $limit = 10, $page = 1, $links = 7, $className ) {
     
	    $this->_limit = $limit;
	    $this->_page = $page;
	    $this->_links = $links;
	    
	    if ( $this->_limit == 'all' ) {
	        $query = $this->_query;
	    } else {
	        $query = $this->_query . " LIMIT " . ( ( $this->_page - 1 ) * $this->_limit ) . ", $this->_limit";
	    }
	    $stmt = $this->db->query( $query );
	    
	    $results = [];
        while($row = $stmt->fetch()) {
            $results[] = new $className($row);
        }

	    $result         = new stdClass();
	    $result->page   = $this->_page;
	    $result->limit  = $this->_limit;
	    $result->total  = $this->_total;
	    $result->data   = $results;
	 
	    return $result;
	}
	public function createLinks ($path,$url) {
		if( $url != '' ){
			parse_str($url, $query_params);
			$url = '';
			if( isset($query_params['category']) ){
				$url .= 'category='.$query_params['category'].'&';
			}
			if( isset($query_params['keyword']) ){
				$url .= 'keyword='.$query_params['keyword'].'&';
			}
		}
	    if ( $this->_limit == 'all' || $this->_total == 0) {
	        return '';
	    }
	    $last = ceil( $this->_total / $this->_limit );
	    $start = ( ( $this->_page - $this->_links ) > 0 ) ? $this->_page - $this->_links : 1;
	    $end = ( ( $this->_page + $this->_links ) < $last ) ? $this->_page + $this->_links : $last;
	 	
	    $html = '<div class="ui center aligned container"><div class="ui pagination menu">';
	 
	    $class = ( $this->_page == 1 ) ? "disabled" : "";
	    $html .= '<a class="' . $class . ' icon item" href="'.$path.'?'.$url.'limit=' . $this->_limit . '&page=' . ( $this->_page - 1 ) . '"><i class="left arrow icon"></i></a>';
	 
	    if ( $start > 1 ) {
	        $html .= '<a class="active item" href="'.$path.'?'.$url.'limit=' . $this->_limit . '&page=1">1</a>';
	        $html .= '<div class="disabled item">...</div>';
	    }
	 
	    for ( $i = $start ; $i <= $end; $i++ ) {
	        $class = ( $this->_page == $i ) ? "active" : "";
	        $html .= '<a class="' . $class . ' item" href="'.$path.'?'.$url.'limit=' . $this->_limit . '&page=' . $i . '">' . $i . '</a>';
	    }
	 
	    if ( $end < $last ) {
	        $html .= '<div class="disabled item">...</div>';
	        $html .= '<a class="item" href="'.$path.'?'.$url.'limit=' . $this->_limit . '&page=' . $last . '">' . $last . '</a>';
	    }
	 
	    $class = ( $this->_page == $last ) ? "disabled" : "";
	    $html .= '<a class="' . $class . ' icon item" href="'.$path.'?'.$url.'limit=' . $this->_limit . '&page=' . ( $this->_page + 1 ) . '"><i class="right arrow icon"></i></a>';
	 
	    $html .= '</div></div>';
	 
	    return $html;
	}
 
}