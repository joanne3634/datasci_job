<?php

class ApplicationMapper extends Mapper
{
    public function save(ApplicationEntity $application) {
        $sql = "insert into application
            (job_id, name, email, phone, latest_company, latest_job_title, gradute_school, gradute_master, experience, english_ability_id, other,highest_education_id,graduate_year,age_year,cv_path,apply_user_id,send_time) values
            (:job_id, :name, :email, :phone, :latest_company, :latest_job_title, :gradute_school, :gradute_master, :experience, :english_ability_id, :other,:highest_education_id,:graduate_year,:age_year,:cv_path,:apply_user_id,:send_time)";
            
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([
            "job_id" => $application->getJobId(),
            "name" => $application->getName(),
            "email" => $application->getEmail(),
            "phone" => $application->getPhone(),
            "latest_company" => $application->get_latest_company(),
            "latest_job_title" => $application->get_latest_job_title(),
            "gradute_school" => $application->get_gradute_school(),
            "gradute_master" => $application->get_gradute_master(),
            "experience" => $application->get_experience(),
            "english_ability_id" => $application->get_english_ability_id(),
            "other" => $application->getOther(),
            "apply_user_id" => $application->get_apply_user_id(),
            "highest_education_id" => $application->get_highest_education_id(),
            "graduate_year" => $application->get_graduate_year(),
            "age_year" => $application->get_age_year(),
            "cv_path" => $application->getCVpath(),
            "send_time" => $application->get_send_time()
        ]);

        if(!$result) {
            throw new Exception("could not save record");
        }
    }    

    public function checkDuplicateApply( $job_id, $apply_user_id ){
        $sql = "SELECT * FROM `application` WHERE `job_id`=".$job_id." and `apply_user_id`=".$apply_user_id;
        // print($sql);
        $stmt = $this->db->query($sql);
        
        if( $stmt->rowCount() == 0 ){
            return array('success'=>'無申請過此工作');
        }else{
            return array('error'=>'您有申請過該工作，請問確定再次寄出職缺申請嗎？');
        }          
    }
    public function getApplicationByUser($user_id){
        $sql = "SELECT j.company, j.job_title, a.* from application a
        join jobs j on (j.id = a.job_id) 
        where a.apply_user_id =".$user_id;
        $stmt = $this->db->query($sql);

        $results = [];
        while($row = $stmt->fetch()) {
            $results[] = new ApplicationEntity($row);
        }
        return $results;  
    }
}