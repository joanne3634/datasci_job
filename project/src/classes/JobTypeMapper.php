<?php

class JobTypeMapper extends Mapper
{
    public function getJobType() {
        $sql = "SELECT id, job_type
            from job_type";
        $stmt = $this->db->query($sql);

        $results = [];
        while($row = $stmt->fetch()) {
            $results[] = new JobTypeEntity($row);
        }
        return $results;
    }

    public function getJobTypeById($id) {
        $sql = "SELECT id, job_type
            from job_type where id = :job_type_id";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(["job_type_id" => $id]);

        return new JobTypeEntity($stmt->fetch());
    }
}

