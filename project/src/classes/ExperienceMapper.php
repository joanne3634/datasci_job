<?php

class ExperienceMapper extends Mapper
{
    public function getExperience() {
        $sql = "SELECT id, experience
            from experience";
        $stmt = $this->db->query($sql);

        $results = [];
        while($row = $stmt->fetch()) {
            $results[] = new ExperienceEntity($row);
        }
        return $results;
    }

    public function getExperienceById($id) {
        $sql = "SELECT id, experience
            from experience where id = :experience_id";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(["experience_id" => $id]);

        return new ExperienceEntity($stmt->fetch());
    }
}

