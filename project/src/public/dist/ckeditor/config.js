/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'about' }
	];

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';

    config.extraPlugins= 'uploadimage,image2';
      // Upload images to a CKFinder connector (note that the response type is set to JSON).
      config.uploadUrl=CKEDITOR.basePath + 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json';

      // Configure your file manager integration. This example uses CKFinder 3 for PHP.
      config.filebrowserBrowseUrl= CKEDITOR.basePath +'ckfinder/ckfinder.html';
      config.filebrowserImageBrowseUrl= CKEDITOR.basePath +'ckfinder/ckfinder.html?type=Images';
      config.filebrowserUploadUrl=CKEDITOR.basePath + 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
      config.filebrowserImageUploadUrl= CKEDITOR.basePath +'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';

      // The following options are not necessary and are used here for presentation purposes only.
      // They configure the Styles drop-down list and widgets to use classes.

      config.stylesSet= [
        { name: 'Narrow image', type: 'widget', widget: 'image', attributes: { 'class': 'image-narrow' } },
        { name: 'Wide image', type: 'widget', widget: 'image', attributes: { 'class': 'image-wide' } }
      ];

      // Load the default contents.css file plus customizations for this sample.
      config.contentsCss= [ CKEDITOR.basePath + 'contents.css', 'http://sdk.ckeditor.com/samples/assets/css/widgetstyles.css' ];

      // Configure the Enhanced Image plugin to use classes instead of styles and to disable the
      // resizer (because image size is controlled by widget styles or the image takes maximum
      // 100% of the editor width).
      config.image2_alignClasses= [ 'image-align-left', 'image-align-center', 'image-align-right' ];
      config.image2_disableResizer= true;

};
